import serial
import time
from xbee import XBee, ZigBee
import Lib_DM_Format
import Lib_DM_Con

def mount_to_send():
    send = []
    send.append(chr(0x7E))
    send.append(chr(0x0))
    send.append(chr(0x11))
    send.append(chr(0x10))
    send.append(chr(0x1))
    send.append(chr(0x00))
    send.append(chr(0x13))
    send.append(chr(0xA2))
    send.append(chr(0x0))
    send.append(chr(0x40))
    send.append(chr(0xDD))
    send.append(chr(0xD8))
    send.append(chr(0x3C))
    send.append(chr(0xFF))
    send.append(chr(0xFE))
    send.append(chr(0x0))
    send.append(chr(0x0))
    send.append(chr(0x4F))
    send.append(chr(0x6C))
    send.append(chr(0x61))
    send.append(chr(0xEF))
    return send

Lib_DM_Con.init_serial()
Lib_DM_Con.init_xbee()

if __name__ == '__main__':
   print """****ECHO TEST ZIGBEE****"""
   while True:
       print "running..."
       time.sleep(1)
       Lib_DM_Con.verify()
       Lib_DM_Con.send_data(mount_to_send())
       time.sleep(1)

